import 'dart:ui';

import 'package:dasf_farm/functionCall/getUsers.dart';
import 'package:dasf_farm/web/addNewUser/addNewUserWindow.dart';
import 'package:dasf_farm/web/getFunction/findUserWithEmail.dart';
import 'package:dasf_farm/web/getFunction/getUsers.dart';
import 'package:dasf_farm/web/organization/organizationModel.dart';
import 'package:dasf_farm/web/otherWidgets/navigationDriver.dart';
import 'package:dasf_farm/web/provaider_objects/organizationP.dart';
import 'package:dasf_farm/web/provaider_objects/userP.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import '../navigationString.dart';
import 'addNewOrganizationLogic.dart';

class AddNewUser extends StatefulWidget {
  @override
  AddNewUserState createState() => AddNewUserState();
}

class AddNewUserState extends State<AddNewUser> {
  NavigationDriverState navigationDriverState = NavigationDriverState();
  AddNewWindowUserState addNewWindowUserState = AddNewWindowUserState();
  TextEditingController findUserWithEmailAndName = TextEditingController();
  var height;
  var width;

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;

    return Material(
      color: Colors.white,
      child: main(context),
    );
  }

  checkStringNavigation(BuildContext context) async {
    var check = context.read<NavigationString>().navigationString;
    if (check == null) {
      var resultGetData = await context
          .read<OrganizationWebModel>()
          .getData(context, statusFindUsers: false);
      var result = await context.read<NavigationString>().getHiveObject();
    } else {
      var resultGetData = await context
          .read<OrganizationWebModel>()
          .getData(context, statusFindUsers: false);
    }
    return "ok";
  }

  Widget main(BuildContext context) {
    return FutureBuilder(
        future: checkStringNavigation(context),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Container(
                height: height,
                width: width,
                color: Colors.white,
                child: Center(
                  child: Center(
                    child: Container(
                      height: height * 0.08,
                      width: height * 0.08,
                      child: Image.asset(
                        "assets/mobile_assets/Иллюстрация_без_названия.gif",
                      ),
                    ),
                  ),
                ),
              ),
            );
          } else {
            return Stack(children: [
              Scaffold(
                backgroundColor: Color.fromRGBO(245, 245, 245, 1),
                body: Row(
                  children: [
                    navigationDriverState.drawerNavigator(
                        context: context,
                        height: height,
                        width: width,
                        person: context.read<UserWeb>().person),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: height * 0.04,
                          left: height * 0.04,
                          right: height * 0.04,
                          bottom: height * 0.04,
                        ),
                        child: Column(
                          children: [
                            SizedBox(
                              height: height * 0.02,
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Пользователи",
                                style: TextStyle(
                                    fontSize: height * 0.03,
                                    fontWeight: FontWeight.w900),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.02,
                            ),
                            filter(),
                            SizedBox(
                              height: height * 0.02,
                            ),
                            Expanded(
                              child: body(),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Consumer<AddNewUserLogic>(
                builder: (BuildContext context, value, Widget child) {
                  if (value.findIserWindow == false) {
                    return Container();
                  } else {
                    return findUserWithEmail();
                  }
                },
              ),
              Consumer<AddNewUserLogic>(
                builder: (BuildContext context, value, Widget child) {
                  if (value.statusAddUser == false) {
                    return Container();
                  } else {
                    return Container(
                      width: width,
                      height: height,
                      color: Color.fromRGBO(0, 0, 0, 0.3),
                      child: Center(
                        child: AddNewWindowUser(),
                      ),
                    );
                  }
                },
              ),
              Consumer<AddNewUserLogic>(
                builder: (BuildContext context, value, Widget child) {
                  if (value.statusSave == false) {
                    return Container();
                  } else {
                    return Container(
                      width: width,
                      height: height,
                      color: Color.fromRGBO(0, 0, 0, 0.3),
                      child: Center(
                        child: Container(
                          height: height * 0.08,
                          width: height * 0.08,
                          child: Image.asset(
                              "assets/mobile_assets/Иллюстрация_без_названия.gif",
                              color: Colors.white),
                        ),
                      ),
                    );
                  }
                },
              ),
            ]);
          }
        });
  }

  Widget filter() {
    return Container(
        padding: EdgeInsets.only(
          top: height * 0.02,
          left: width * 0.02,
          right: width * 0.05,
          bottom: height * 0.02,
        ),
        height: height * 0.3,
        width: width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(height * 0.006),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "ФИЛЬТР",
                  style: TextStyle(
                      fontSize: height * 0.03, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: height * 0.04,
                ),
                inputData(
                    textEditingController:
                        context.read<AddNewUserLogic>().nameUser,
                    text: "G-mail почта",
                    textHint: "Введите почту"),
                SizedBox(
                  height: height * 0.02,
                ),
                Container(
                  alignment: Alignment.center,
                  width: width * 0.2,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            context.read<AddNewUserLogic>().reverseNameUser();
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(height),
                              border: Border.all(
                                width: width * 0.001,
                                color: Color.fromRGBO(48, 118, 187, 1),
                              ),
                            ),
                            child: Padding(
                              padding: EdgeInsets.only(
                                top: height * 0.011,
                                bottom: height * 0.011,
                                left: height * 0.015,
                                right: height * 0.015,
                              ),
                              child: Text(
                                "СБРОСИТЬ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    color: Color.fromRGBO(48, 118, 187, 1),
                                    fontSize: height * 0.015),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: width * 0.02,
                        ),
                        buttonAddOrganization(
                            textButton: "ПОИСК В БАЗЕ ДАННЫХ",
                            function: () async {
                              context.read<AddNewUserLogic>().statusSaveE();
                              var result =
                                  await GetUserWithEmail.getUserWithEmail(
                                      context
                                          .read<AddNewUserLogic>()
                                          .nameUser
                                          .text,
                                      context);

                              context.read<AddNewUserLogic>().statusSaveE();
                            }),
                        SizedBox(
                          width: width * 0.01,
                        ),
                      ]),
                ),
              ],
            ),
            Container(
              width: width * 0.4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: height * 0.08,
                    width: height * 0.08,
                    child: SvgPicture.asset(
                      "assets/mobile_assets/ic_search_filled.svg",
                      color: Color.fromRGBO(58, 63, 67, 0.5),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "Для быстрого поиска пользователя введите g-mail в строку пользователя",
                    style: TextStyle(fontSize: height * 0.02),
                  )
                ],
              ),
            ),
          ],
        ));
  }

  Widget inputData(
      {var text,
      var textEditingController,
      var focusNode,
      var textHint,
      Function function}) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          "$text",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: height * 0.019,
          ),
        ),
      ),
      SizedBox(
        height: height * 0.02,
      ),
      Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: width * 0.2,
            height: height * 0.06,
            color: Colors.white,
            child: Form(
              child: TextFormField(
                onChanged: (value) {
                  function(value);
                },
                style: TextStyle(fontSize: height * 0.023),
                focusNode: focusNode,
                controller: textEditingController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(
                      top: height * 0.02,
                      bottom: height * 0.02,
                      left: width * 0.01),
                  hintStyle: TextStyle(fontSize: height * 0.019),
                  hintText: "$textHint",
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: height * 0.003,
                      color: Color.fromRGBO(48, 118, 187, 1),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(58, 63, 67, 0.5),
                        width: height * 0.003),
                  ),
                ),
              ),
              // child: Consumer<T>(
              //     builder: (context, value, child) {
              //   return
              // }),
            ),
          ),
        ],
      ),
      SizedBox(
        height: height * 0.015,
      ),
    ]);
  }

  Widget body() {
    return Container(
      padding: EdgeInsets.all(height * 0.02),
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(height * 0.006),
      ),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Consumer<AddNewUserLogic>(
                  builder: (BuildContext context, value, Widget child) {
                    if (value.findUser == true) {
                      return buttonAddOrganization(
                          textButton: "ПОИСК ПОЛЬЗОВАТЕЛЯ",
                          function: () {
                            context
                                .read<AddNewUserLogic>()
                                .findIserWindowW(true);
                          });
                    } else {
                      return Container();
                    }
                  },
                ),
                SizedBox(
                  width: width * 0.02,
                ),
                buttonAddOrganization(
                    textButton: "ДОБАВИТЬ ПОЛЬЗОВАТЕЛЯ",
                    function: () {
                      context
                          .read<AddNewUserLogic>()
                          .statusFindOrSaveUserR(false);
                      context.read<AddNewUserLogic>().statusAddUserS();
                    }),
                SizedBox(
                  width: width * 0.02,
                ),
                buttonAddOrganization(
                    textButton: "ПОКАЗАТЬ ВСЕХ ПОЛЬЗОВАТЕЛЕЙ",
                    function: () async {
                      context.read<AddNewUserLogic>().statusSaveE();
                      var result = await GetUsersWeb.getUsers(context);
                      context.read<AddNewUserLogic>().statusSaveE();
                    }),
              ],
            ),
            SizedBox(
              height: height * 0.02,
            ),
            Expanded(
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(height * 0.006),
                    color: Color.fromRGBO(245, 245, 245, 1),
                  ),
                  height: height * 0.4,
                  padding: EdgeInsets.only(
                      top: height * 0.02,
                      left: height * 0.03,
                      right: height * 0.03,
                      bottom: height * 0.001),
                  width: width,
                  child: Consumer<AddNewUserLogic>(
                    builder: (BuildContext context, value, Widget child) {
                      if (value.listUser.length == 0) {
                        return Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: height * 0.08,
                                width: height * 0.08,
                                child: SvgPicture.asset(
                                  "assets/mobile_assets/ic_farmer_empty-1.svg",
                                  color: Color.fromRGBO(58, 63, 67, 0.5),
                                ),
                              ),
                              SizedBox(
                                height: height * 0.02,
                              ),
                              Text(
                                "Здусь будут отображаться пользователи. Для отображение всех пользователей нажмите на кнопку\n<<показать всех пользователей>>, или введите g-mail в строку поиска\n для отображения конкретного пользователя",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: height * 0.02,
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return ListView(
                            padding: EdgeInsets.only(bottom: height * 0.02),
                            children: value.listUser.map<Widget>((e) {
                              return Column(
                                children: [
                                  SizedBox(
                                    height: height * 0.02,
                                  ),
                                  cardFerm(objectUser: e)
                                ],
                              );
                            }).toList());
                      }
                    },
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget buttonAddOrganization({var textButton, Function function}) {
    return Container(
      height: height * 0.04,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
            Color.fromRGBO(48, 118, 187, 1),
          ),
          textStyle: MaterialStateProperty.all<TextStyle>(
            TextStyle(
              fontWeight: FontWeight.w900,
            ),
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(height),
            ),
          ),
        ),
        onPressed: () {
          function();
        },
        child: Padding(
          padding: EdgeInsets.all(height * 0.012),
          child: Text(
            "$textButton",
            style: TextStyle(
                fontWeight: FontWeight.w900,
                color: Colors.white,
                fontSize: height * 0.015),
          ),
        ),
      ),
    );
  }

  FocusNode focusNode = FocusNode();

  Widget findUserWithEmail() {
    return Container(
        height: height,
        width: width,
        color: Color.fromRGBO(0, 0, 0, 0.3),
        child: Center(
          child: Container(
            height: height * 0.6,
            width: width * 0.6,
            padding: EdgeInsets.all(height * 0.02),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(height * 0.02),
                color: Colors.white),
            child: Stack(children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Поиск пользователя",
                    style: TextStyle(
                        fontSize: height * 0.04, fontWeight: FontWeight.w900),
                  ),
                  SizedBox(
                    height: height * 0.04,
                  ),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Строка поиска",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: height * 0.019,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: height * 0.02,
                        ),
                        Container(
                          width: width * 0.2,
                          height: height * 0.06,
                          color: Colors.white,
                          child: Form(
                            child: TextFormField(
                              onChanged: (value) {
                                context
                                    .read<AddNewUserLogic>()
                                    .findUserWithNameAndEmail(value);
                              },
                              style: TextStyle(fontSize: height * 0.023),
                              focusNode: focusNode,
                              controller: findUserWithEmailAndName,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                    top: height * 0.02,
                                    bottom: height * 0.02,
                                    left: width * 0.01),
                                hintStyle: TextStyle(fontSize: height * 0.019),
                                hintText: "Введите почту или имя",
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: height * 0.003,
                                    color: Color.fromRGBO(48, 118, 187, 1),
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color.fromRGBO(58, 63, 67, 0.5),
                                      width: height * 0.003),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ]),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(height * 0.02),
                      color: Color.fromRGBO(245, 245, 245, 1),
                      child: Consumer<AddNewUserLogic>(
                        builder: (BuildContext context, value, Widget child) {
                          if (value.listSort.length == 0) {
                            return Container(
                              height: height * 0.4,
                              width: width,
                              child: ListView(
                                children: value.listUser.map<Widget>((e) {
                                  return Column(
                                    children: [
                                      SizedBox(
                                        height: height * 0.02,
                                      ),
                                      cardFerm(objectUser: e),
                                    ],
                                  );
                                }).toList(),
                              ),
                            );
                          } else {
                            return Container(
                              height: height * 0.4,
                              width: width,
                              child: ListView(
                                children: value.listSort.map<Widget>((e) {
                                  return Column(
                                    children: [
                                      SizedBox(
                                        height: height * 0.02,
                                      ),
                                      cardFerm(objectUser: e),
                                    ],
                                  );
                                }).toList(),
                              ),
                            );
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.topRight,
                child: buttonRedactorClose(),
              ),
            ]),
          ),
        ));
  }

  Widget buttonRedactorClose() {
    return Container(
      height: height * 0.05,
      width: height * 0.05,
      child: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.all(height * 0.003)),
            backgroundColor: MaterialStateProperty.all(
              Color.fromRGBO(48, 118, 187, 1),
            ),
            textStyle: MaterialStateProperty.all<TextStyle>(
              TextStyle(
                fontWeight: FontWeight.w900,
              ),
            ),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(height),
              ),
            ),
          ),
          onPressed: () {
            context.read<AddNewUserLogic>().findIserWindowW(false);
          },
          child: Container(
            height: height * 0.05,
            width: height * 0.05,
            child: Icon(
              Icons.close,
              size: height * 0.04,
            ),
          ),
        ),
      ),
    );
  }

  Widget cardFerm({ObjectUser objectUser}) {
    return Container(
      color: Colors.white,
      child: InkWell(
        onTap: () {},
        child: Container(
          width: width,
          height: height * 0.077,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(height * 0.006)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: width * 0.01,
                  ),
                  Container(
                    height: height * 0.035,
                    width: height * 0.035,
                    child: SvgPicture.asset(
                      "assets/mobile_assets/ic_user.svg",
                      color: Color.fromRGBO(48, 118, 187, 1),
                    ),
                  ),
                  SizedBox(
                    width: width * 0.005,
                  ),
                  Text(
                    "${objectUser.name}",
                    style: TextStyle(
                        fontSize: height * 0.021, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    width: width * 0.005,
                  ),
                  Text(
                    "${objectUser.email}",
                    style: TextStyle(
                        fontSize: height * 0.021, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
              Container(
                child: Row(children: [
                  // buttonAddOrganization(
                  //     textButton: "УДАЛИТЬ",
                  //     function: () {
                  //       // context.read<OrganizationP>().addNewOrgStatus();
                  //     }),
                  SizedBox(
                    width: width * 0.01,
                  ),
                  buttonAddOrganization(
                      textButton: "РЕДАКТИРОВАТЬ",
                      function: () {
                        context
                            .read<AddNewUserLogic>()
                            .saveUserUid(objectUser.uid);
                        context
                            .read<AddNewUserLogic>()
                            .statusFindOrSaveUserR(true);
                        context
                            .read<AddNewUserLogic>()
                            .initializeObjectUser(objectUser);
                        context.read<AddNewUserLogic>().statusAddUserS();
                      }),
                  SizedBox(
                    width: width * 0.01,
                  ),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buttonRedactor() {
    return Container(
      height: height * 0.038,
      width: height * 0.038,
      child: ElevatedButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(EdgeInsets.all(height * 0.018)),
          backgroundColor: MaterialStateProperty.all(
            Color.fromRGBO(48, 118, 187, 1),
          ),
          textStyle: MaterialStateProperty.all<TextStyle>(
            TextStyle(
              fontWeight: FontWeight.w900,
            ),
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(height),
            ),
          ),
        ),
        onPressed: () {},
        child: Container(
          height: height * 0.04,
          width: height * 0.04,
          child: SvgPicture.asset("assets/mobile_assets/ic_edit.svg",
              color: Colors.white),
        ),
      ),
    );
  }
}
