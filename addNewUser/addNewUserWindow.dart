import 'package:dasf_farm/functionCall/setUser.dart';
import 'package:dasf_farm/functionCall/updateUserById.dart';
import 'package:dasf_farm/web/getFunction/findUserWithEmail.dart';
import 'package:dasf_farm/web/getFunction/getUsers.dart';
import 'package:dasf_farm/web/provaider_objects/organizationP.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'addNewOrganizationLogic.dart';

class AddNewWindowUser extends StatefulWidget {
  @override
  AddNewWindowUserState createState() => AddNewWindowUserState();
}

class AddNewWindowUserState extends State<AddNewWindowUser> {
  var height;
  var width;

  TextEditingController inputNaimenovanie = TextEditingController();
  FocusNode focusNodeInputNaimenovanie = FocusNode();
  AnimationController controllerTransform;

  Widget buttonRedactor() {
    return Container(
      height: height * 0.05,
      width: height * 0.05,
      child: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.all(height * 0.003)),
            backgroundColor: MaterialStateProperty.all(
              Color.fromRGBO(48, 118, 187, 1),
            ),
            textStyle: MaterialStateProperty.all<TextStyle>(
              TextStyle(
                fontWeight: FontWeight.w900,
              ),
            ),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(height),
              ),
            ),
          ),
          onPressed: () {
            context.read<AddNewUserLogic>().statusFindOrSaveUserR(false);
            context.read<AddNewUserLogic>().statusAddUserS();
            context.read<AddNewUserLogic>().functiaNullData();
          },
          child: Container(
            height: height * 0.05,
            width: height * 0.05,
            child: Icon(
              Icons.close,
              size: height * 0.04,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      height: height * 0.55,
      width: width * 0.53,
      padding: EdgeInsets.all(height * 0.04),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(height * 0.006)),
      child: Stack(children: [
        Container(
          // color: Colors.red,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "Добавление пользователя",
                style: TextStyle(
                    fontSize: height * 0.04, fontWeight: FontWeight.w900),
              ),
              SizedBox(
                height: height * 0.05,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      // color: Colors.red,
                      height: height * 0.36,
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          inputData(
                              textEditingController:
                                  context.read<AddNewUserLogic>().nameUser,
                              text: "ФИО",
                              textHint: "Введите ФИО"),
                          inputData(
                              textEditingController:
                                  context.read<AddNewUserLogic>().contactInfo,
                              text: "Контактный номер",
                              textHint: "Введите номер телефона"),
                          inputData(
                              textEditingController:
                                  context.read<AddNewUserLogic>().gmail,
                              text: "G-mail почта",
                              textHint: "Введите почту"),
                        ],
                      ),
                    ),
                    // Container(
                    //   height: height * 0.36,
                    //   child: Column(
                    //     // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       inputData(
                    //           textEditingController:
                    //               context.read<AddNewUserLogic>().gmail,
                    //           text: "G-mail почта",
                    //           textHint: "Введите имя"),
                    //     ],
                    //   ),
                    // ),
                    Padding(
                      padding: EdgeInsets.only(right: width * 0.02),
                      child: Container(
                        height: height * 0.36,
                        width: width * 0.2,
                        child: Center(
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: height * 0.08,
                              ),
                              Container(
                                height: height * 0.1,
                                width: height * 0.1,
                                child: SvgPicture.asset(
                                  "assets/mobile_assets/ic_warning_empty.svg",
                                  color: Color.fromRGBO(58, 63, 67, 0.5),
                                ),
                              ),
                              SizedBox(
                                height: height * 0.02,
                              ),
                              Text(
                                "Всегда проверяйте актуальность\nG-mail почты пользователя",
                                style: TextStyle(
                                  fontSize: height * 0.025,
                                ),
                                textAlign: TextAlign.center,
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [buttonRedactor()]),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: height * 0.05,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(48, 118, 187, 1),
                    ),
                    textStyle: MaterialStateProperty.all<TextStyle>(
                      TextStyle(
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(height),
                      ),
                    ),
                  ),
                  onPressed: () async {
                    ObjectUser objectUser = ObjectUser();
                    objectUser.email =
                        context.read<AddNewUserLogic>().gmail.text;
                    objectUser.language = "ru";
                    objectUser.name =
                        fio(context.read<AddNewUserLogic>().nameUser.text, 0);

                    objectUser.phone =
                        context.read<AddNewUserLogic>().contactInfo.text;
                    objectUser.surname =
                        fio(context.read<AddNewUserLogic>().nameUser.text, 1);
                    // context.read<AddNewUserLogic>().surnameUser.text;
                    objectUser.uid = context.read<AddNewUserLogic>().uidUser;
                    var proverka = proverkaNull(objectUser: objectUser);
                    if (proverka == "error") {
                    } else {
                      context.read<AddNewUserLogic>().statusSaveE();
                      if (context
                              .read<AddNewUserLogic>()
                              .statusFindOrSaveUser ==
                          true) {
                        UpdateUserById updateUser = UpdateUserById();
                        var result =
                            await updateUser.updateUserById(objectUser);
                        var d;
                        context
                            .read<AddNewUserLogic>()
                            .statusFindOrSaveUserR(false);
                        var resultGetUserEmail =
                            await GetUserWithEmail.getUserWithEmail(
                                context.read<AddNewUserLogic>().emailUser,
                                context);
                      } else {
                        SetUser setUser = SetUser();
                        var result = await setUser.setUser(objectUser);
                        var d;
                        var resultGetUser = await GetUsersWeb.getUsers(context);
                      }
                      context.read<AddNewUserLogic>().statusAddUserS();
                      context.read<OrganizationP>().saveStatusSubOrgS();
                      context.read<AddNewUserLogic>().statusSaveE();
                      context.read<AddNewUserLogic>().functiaNullData();
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.all(height * 0.010),
                    child: Text(
                      "Сохранить",
                      style: TextStyle(
                          fontWeight: FontWeight.w800,
                          color: Colors.white,
                          fontSize: height * 0.018),
                    ),
                  ),
                ),
              ),
            ],
          )
        ]),
      ]),
    );
  }

  fio(String fio, int index) {
    List<String> mylist = fio.split(' ');
    switch (index) {
      case 0:
        return mylist[0];
        break;
      case 1:
        return "${mylist[1]} ${mylist[2]}";
        break;
    }
  }

  proverkaNull({ObjectUser objectUser}) {
    if (objectUser.name == "" ||
        objectUser.email == "" ||
        objectUser.surname == "" ||
        objectUser.phone == "" ||
        objectUser.language == "") {
      return "error";
    } else {
      return "OK";
    }
  }

  Widget listCheckBox() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            checkBox(),
            SizedBox(
              width: width * 0.01,
            ),
            Text(
              "Взвешивание",
              style: TextStyle(fontSize: height * 0.02),
            )
          ]),
          SizedBox(
            height: height * 0.015,
          ),
          Row(children: [
            checkBox(),
            SizedBox(
              width: width * 0.01,
            ),
            Text(
              "Перевод",
              style: TextStyle(fontSize: height * 0.02),
            )
          ]),
          SizedBox(
            height: height * 0.015,
          ),
          Row(children: [
            checkBox(),
            SizedBox(
              width: width * 0.01,
            ),
            Text(
              "Выбытие",
              style: TextStyle(fontSize: height * 0.02),
            )
          ]),
        ],
      ),
    );
  }

  Widget checkBox() {
    return Container(
      height: height * 0.04,
      width: height * 0.04,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(height * 0.004),
        border: Border.all(
          width: height * 0.003,
          color: Color.fromRGBO(58, 63, 67, 0.5),
        ),
      ),
      child: Checkbox(
        value: true,
        onChanged: (bool value) {},
      ),
    );
  }

  Widget inputData(
      {var text, var textEditingController, var focusNode, var textHint}) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          "$text",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: height * 0.019,
          ),
        ),
      ),
      SizedBox(
        height: height * 0.02,
      ),
      Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: width * 0.2,
            height: height * 0.06,
            color: Colors.white,
            child: Form(
              child: TextFormField(
                style: TextStyle(fontSize: height * 0.023),
                focusNode: focusNode,
                controller: textEditingController,
                keyboardType: TextInputType.number,
                // inputFormatters: [
                //   FilteringTextInputFormatter.allow(
                //       RegExp(r'^([A-Z]{1}[a-z]{1,15})$')),
                // ],
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(
                      top: height * 0.02,
                      bottom: height * 0.02,
                      left: width * 0.01),
                  hintStyle: TextStyle(fontSize: height * 0.019),
                  hintText: "$textHint",
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: height * 0.003,
                      color: Color.fromRGBO(48, 118, 187, 1),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(58, 63, 67, 0.5),
                        width: height * 0.003),
                  ),
                ),
              ),
              // child: Consumer<T>(
              //     builder: (context, value, child) {
              //   return
              // }),
            ),
          ),
        ],
      ),
      SizedBox(
        height: height * 0.015,
      ),
    ]);
  }

  Widget conter(
      {var statusAnim,
      BuildContext context,
      bool widgetListBool = false,
      var widgetList,
      var textNow,
      var height,
      var width,
      var controllerTransform,
      void Function() onTTap}) {
    return Container(
      child: Column(
        children: [
          InkWell(
            onTap: () => onTTap(),
            child: Container(
              width: width * 0.2,
              height: height * 0.06,
              alignment: Alignment.centerLeft,
              // width: width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(height * 0.005),
                  topRight: Radius.circular(height * 0.005),
                  bottomLeft: statusAnim
                      ? Radius.circular(0)
                      : Radius.circular(height * 0.005),
                  bottomRight: statusAnim
                      ? Radius.circular(0)
                      : Radius.circular(height * 0.005),
                ),
                border: Border.all(
                    color: statusAnim
                        ? Color.fromRGBO(48, 118, 187, 1)
                        : Color.fromRGBO(58, 63, 67, 0.5),
                    width: height * 0.003),
                color: Colors.white,
              ),
              padding: EdgeInsets.only(
                left: width * 0.01,
                right: width * 0.01,
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      textNow != null ? textNow : "Не выбрано",
                      style: TextStyle(
                        fontSize: height * 0.019,
                        color: Color.fromRGBO(58, 63, 67, 0.8),
                      ),
                    ),
                    InkWell(
                      // onTap: () => onTTap(),
                      child: RotationTransition(
                        turns: Tween(begin: 0.0, end: 0.5)
                            .animate(controllerTransform),
                        child: Container(
                          child: SvgPicture.asset(
                            "assets/mobile_assets/ic_arrow_down.svg",
                            height: height * 0.03,
                            width: height * 0.03,
                            color: Color.fromRGBO(48, 118, 187, 1),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          statusAnim
              ? Container(
                  padding: EdgeInsets.only(
                      left: width * 0.025,
                      top: height * 0.02,
                      bottom: height * 0.02,
                      right: width * 0.05),
                  width: width * 0.2,
                  height: height * 0.12,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      left: BorderSide(
                          color: Color.fromRGBO(48, 118, 187, 1),
                          width: height * 0.003),
                      right: BorderSide(
                          color: Color.fromRGBO(48, 118, 187, 1),
                          width: height * 0.003),
                      bottom: BorderSide(
                          color: Color.fromRGBO(48, 118, 187, 1),
                          width: height * 0.003),
                    ),
                  ),
                  child: widgetListBool ? widgetList : Container(),
                )
              : Container(),
        ],
      ),
    );
  }
}
