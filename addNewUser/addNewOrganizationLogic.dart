import 'package:flutter/widgets.dart';

class AddNewUserLogic extends ChangeNotifier {
  List<ObjectUser> listUser = [];

  var statusFindOrSaveUser = false;

  var findUser = false;
  var findIserWindow = false;

  void findIserWindowW(bool status) {
    findIserWindow = status;
    notifyListeners();
  }

  void findUserR(bool status) {
    findUser = status;
    notifyListeners();
  }

  statusFindOrSaveUserR(var status) {
    statusFindOrSaveUser = status;
    notifyListeners();
  }

  ObjectUser objectUser;

  initializeObjectUser(var objectUser) {
    this.objectUser = objectUser;
    nameUser.text = objectUser.name;
    surnameUser.text = objectUser.surname;
    contactInfo.text = objectUser.phone;
    gmail.text = objectUser.email;
    emailUser = objectUser.email;
    notifyListeners();
  }

  addUserToList(var list) {
    listUser = [];
    for (var i = 0; i < list.length; i++) {
      ObjectUser objectUser = ObjectUser();
      objectUser.email = list[i]["email"];
      objectUser.language = list[i]["language"];
      objectUser.name = list[i]["name"];
      objectUser.phone = list[i]["phone"];
      objectUser.uid = list[i]["uid"];
      objectUser.surname = list[i]["surname"];
      listUser.add(objectUser);
    }
    findUser = true;
    notifyListeners();
  }

  List<ObjectUser> listSort = [];

  findUserWithNameAndEmail(String string) {
    listSort = [];
    listUser.forEach((element) {
      if (element.email.contains(string)) {
        listSort.add(element);
      } else if (element.name.contains(string)) {
        listSort.add(element);
      }
    });
    notifyListeners();
  }

  var uidUser;
  var emailUser;

  saveUserUid(var uid) {
    uidUser = uid;
    notifyListeners();
  }

  addNewIteam(var map) {
    listUser = [];
    ObjectUser objectUser = ObjectUser();
    objectUser.email = map["email"];
    objectUser.language = map["language"];
    objectUser.name = map["name"];
    objectUser.phone = map["phone"];
    objectUser.uid = map["uid"];
    objectUser.surname = map["surname"];
    uidUser = map["uid"];
    listUser.add(objectUser);
    notifyListeners();
  }

  var statusAddUser = false;

  var statusSave = false;

  statusSaveE() {
    statusSave = !statusSave;
    notifyListeners();
  }

  statusAddUserS() {
    statusAddUser = !statusAddUser;
    notifyListeners();
  }

  bool statusAddNewSubOraganization = false;

  var id_sub_organization;
  var idOrganization;

  var statusSizeInput = false;

  reverseNameUser() {
    nameUser.text = "";
    notifyListeners();
  }

  TextEditingController nameUser = TextEditingController();
  TextEditingController surnameUser = TextEditingController();
  TextEditingController contactInfo = TextEditingController();
  TextEditingController gmail = TextEditingController();

  functiaSizeInput() {
    statusSizeInput = !statusSizeInput;
    notifyListeners();
  }

  newOrganizationAdd() {
    statusAddNewSubOraganization = !statusAddNewSubOraganization;
    notifyListeners();
  }

  addIdOrganization({var id_sub_organization}) {
    this.id_sub_organization = id_sub_organization;
    notifyListeners();
  }

  functiaNullData() {
    nameUser.text = "";
    surnameUser.text = "";
    contactInfo.text = "";
    gmail.text = "";
    notifyListeners();
  }
}

class ObjectUser {
  var language;
  var phone;
  var name;
  var surname;
  var email;
  var uid;
}
