import 'package:decimal/decimal.dart';
import 'package:flutter/cupertino.dart';

class AddNewCowLogicWeb extends ChangeNotifier {
  var save = false;
  double weight = 0.0;
  var statusDead = false;
  var statusFindCow = 0;
  var valuesOk = false;

  valuesOkK() {
    valuesOk = !valuesOk;
    notifyListeners();
  }

  saveE() {
    save = !save;
    notifyListeners();
  }

  TextEditingController textFind = TextEditingController();
  TextEditingController textWeight = TextEditingController(text: "0.0");

  String gender = "QxlG49dRxQrQg9H0RnDE";
  String nameTelochka = "Телочка";
  String nameBuc = "Бычок";

  var statusGendetTot = "";

  // void addWeightText({String weight}) {
  //   var number = int.parse(weight);
  //   this.weight = number;
  //   notifyListeners();
  // }

  functiaFindCowStatus() {
    statusFindCow = 1;
    notifyListeners();
  }

  functiaDontFindCowStatus() {
    statusFindCow = 0;
    notifyListeners();
  }

  @override
  functiaDeletData() {
    gender = "QxlG49dRxQrQg9H0RnDE";
    notifyListeners();
  }

  functionAddGender({var idGroup}) {
    if (idGroup != "UbFdMGuVeySjRTvngpDd") {
      nameTelochka = "Телка";
      nameBuc = "Бык";
      if (gender == "QxlG49dRxQrQg9H0RnDE" ||
          gender == "vkYVpkLimiyoYGXeSJKr") {
        gender = "vkYVpkLimiyoYGXeSJKr";
        statusGendetTot = "Телка";
        notifyListeners();
      } else {
        statusGendetTot = "Бык";
        gender = "7LGy1b7FncEaJNKX50CN";
        notifyListeners();
      }
      notifyListeners();
    } else {
      nameTelochka = "Телочка";
      nameBuc = "Бычок";
      if (gender == "vkYVpkLimiyoYGXeSJKr" ||
          gender == "QxlG49dRxQrQg9H0RnDE") {
        gender = "QxlG49dRxQrQg9H0RnDE";
        statusGendetTot = "Телочка";
        notifyListeners();
      } else {
        statusGendetTot = "Бычок";
        gender = "swLS6KyxrzBu0NEtgTgx";
        notifyListeners();
      }
      notifyListeners();
    }
  }

  void statusDeadBool() {
    statusDead = !statusDead;
    notifyListeners();
  }

  var idGroup = "UbFdMGuVeySjRTvngpDd";
  var nameGroup = "Теленнок профилактория";

  saveGroup({var idGroup, var nameGroup}) {
    this.idGroup = idGroup;
    this.nameGroup = nameGroup;
    notifyListeners();
  }

  var statusSizeInput1 = false;
  var statusSizeInput = false;
  functiaSizeInput1() {
    statusSizeInput1 = !statusSizeInput1;
    notifyListeners();
  }

  functiaSizeInput() {
    statusSizeInput = !statusSizeInput;
    notifyListeners();
  }

  var idSpecies;
  var nameSpecies;
  functionSaveSubOrganization({var idSpecies, var nameSpecies}) {
    this.idSpecies = idSpecies;
    this.nameSpecies = nameSpecies;
    notifyListeners();
  }

  var startDay = 0;

  FocusNode focusNodeWeight = FocusNode();

  void addWeight() {
    if (textWeight.text == "") {
      var number = 0.0;
      number = number + 1.0;
      weight = number;
      textWeight.text = number.toString();
      notifyListeners();
    } else {
      var number = double.parse(textWeight.text);
      number = number + 1.0;
      weight = number;
      textWeight.text = number.toString();
      notifyListeners();
    }
  }

  void removeWeight() {
    if (textWeight.text == "") {
    } else {
      var number = double.parse(textWeight.text);
      if (number == 0.0) {
      } else {
        if (number < 1.0) {
          number = number - number;
          weight = number;
          textWeight.text = number.toString();
          notifyListeners();
        } else {
          var dec = Decimal.parse("$number") - Decimal.parse("1.0");
          var decDouble = double.parse(dec.toString());
          weight = decDouble;
          textWeight.text = decDouble.toString();
        }
      }
      notifyListeners();
    }
  }
}
