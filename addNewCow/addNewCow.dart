import 'dart:async';

import 'package:dasf_farm/functionCall/saveCowAllParametrs.dart';
import 'package:dasf_farm/mobile_view/hiveObject/groups/groups.dart';
import 'package:dasf_farm/mobile_view/hiveObject/moduls/modulsDataBase.dart';
import 'package:dasf_farm/mobile_view/hiveObject/species/species.dart';
import 'package:dasf_farm/mobile_view/hiveObject/userHive.dart';
import 'package:dasf_farm/mobile_view/widgets/widgets_model.dart';
import 'package:dasf_farm/web/addNewCow/addNewCowLogic.dart';
import 'package:dasf_farm/web/addNewUser/addNewUser.dart';
import 'package:dasf_farm/web/addNewUser/addNewUserWindow.dart';
import 'package:dasf_farm/web/getFunction/getStatic.dart';
import 'package:dasf_farm/web/organization/organizationModel.dart';
import 'package:dasf_farm/web/otherWidgets/navigationDriver.dart';
import 'package:dasf_farm/web/otherWidgets/okWinow.dart';
import 'package:dasf_farm/web/otherWidgets/staticWindow.dart';
import 'package:dasf_farm/web/provaider_objects/staticsP.dart';
import 'package:dasf_farm/web/provaider_objects/userP.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../../allParametrsCow.dart';
import '../navigationString.dart';

class AddNewCowWeb extends StatefulWidget {
  @override
  _AddNewCowLogicState createState() => _AddNewCowLogicState();
}

class _AddNewCowLogicState extends State<AddNewCowWeb>
    with TickerProviderStateMixin {
  OkWindow okWindow = OkWindow();
  NavigationDriverState navigationDriverState = NavigationDriverState();
  AddNewWindowUserState addNewWindowUserState = AddNewWindowUserState();
  AddNewUserState addNewUserState = AddNewUserState();
  var startDay = "01";
  var startMounth = "01";
  var startYers = "01";
  var timeStamp = 0;
  DateTime datetime = DateTime.now();
  Timer timer;
  FocusNode focusNode = FocusNode();
  FocusNode focusNode1 = FocusNode();
  FocusNode focusNode2 = FocusNode();
  FocusNode focusNode3 = FocusNode();
  AnimationController _controller2;
  Animation<Offset> _animation2;
  StaticData staticData = StaticData();
  var height;
  var width;

  TextEditingController textEditingController = TextEditingController();
  TextEditingController textEditingController1 = TextEditingController();
  TextEditingController textEditingController2 = TextEditingController();
  TextEditingController textEditingController3 = TextEditingController();

  WidgetsCustomState widgetsCustomState = WidgetsCustomState();

  AnimationController controllerTransform;
  AnimationController controllerTransform1;

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Material(child: main());
  }

  @override
  void initState() {
    super.initState();
    startTimeStamp = datetime.millisecondsSinceEpoch;
    focusNode.requestFocus();
    _controller2 = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );
    _animation2 = Tween<Offset>(
      begin: Offset(0, 1),
      end: Offset(0, 0),
    ).animate(
      CurvedAnimation(
        parent: _controller2,
        curve: Curves.easeInCubic,
      ),
    );
    startDay = datetime.day.toString();
    startMounth = datetime.month.toString();
    startYers = datetime.year.toString();
    timeStamp = datetime.millisecondsSinceEpoch;

    controllerTransform = AnimationController(
      duration: const Duration(milliseconds: 100),
      vsync: this,
    );
    controllerTransform1 = AnimationController(
      duration: const Duration(milliseconds: 100),
      vsync: this,
    );
  }

  checkStringNavigation(BuildContext context) async {
    var check = context.read<NavigationString>().navigationString;
    if (check == null) {
      var resultGetData = await context.read<OrganizationWebModel>().getData(
            context,
          );
      var result = await context.read<NavigationString>().getHiveObject();
    } else {
      var resultGetData = await context.read<OrganizationWebModel>().getData(
            context,
          );
    }
    return "ok";
  }

  Widget main() {
    return FutureBuilder(
        future: checkStringNavigation(context),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Container(
                height: height,
                width: width,
                color: Colors.white,
                child: Center(
                  child: Center(
                    child: Container(
                      height: height * 0.08,
                      width: height * 0.08,
                      child: Image.asset(
                        "assets/mobile_assets/Иллюстрация_без_названия.gif",
                      ),
                    ),
                  ),
                ),
              ),
            );
          } else {
            return Stack(children: [
              Scaffold(
                backgroundColor: Color.fromRGBO(245, 245, 245, 1),
                body: Row(
                  children: [
                    navigationDriverState.drawerNavigator(
                        context: context,
                        height: height,
                        width: width,
                        person: context.read<UserWeb>().person),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: height * 0.04,
                          left: height * 0.04,
                          right: height * 0.04,
                          bottom: height * 0.04,
                        ),
                        child: Column(
                          children: [
                            SizedBox(
                              height: height * 0.02,
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "${context.read<NavigationString>().navigationString}",
                                style: TextStyle(
                                    fontSize: height * 0.03,
                                    fontWeight: FontWeight.w900),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.02,
                            ),
                            filter(),
                            SizedBox(
                              height: height * 0.02,
                            ),
                            Expanded(
                              child: body(),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Consumer<AddNewCowLogicWeb>(
                builder: (BuildContext context, value, Widget child) {
                  if (value.save == false) {
                    return Container();
                  } else {
                    return Center(
                      child: Container(
                        height: height,
                        width: width,
                        color: Color.fromRGBO(0, 0, 0, 0.3),
                        child: Center(
                          child: Container(
                            height: height * 0.08,
                            width: height * 0.08,
                            child: Image.asset(
                              "assets/mobile_assets/Иллюстрация_без_названия.gif",
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                },
              ),
              Consumer<AddNewCowLogicWeb>(
                builder: (BuildContext context, value, Widget child) {
                  if (value.valuesOk == false) {
                    return Container();
                  } else {
                    return Container(
                      height: height,
                      width: width,
                      color: Color.fromRGBO(0, 0, 0, 0.3),
                      child: Center(
                        child: okWindow.okOkno(height, width, () {
                          context.read<AddNewCowLogicWeb>().valuesOkK();
                        }),
                      ),
                    );
                  }
                },
              )
            ]);
          }
        });
  }

  Widget filter() {
    return Container(
        padding: EdgeInsets.only(
          top: height * 0.02,
          left: width * 0.02,
          right: width * 0.05,
          bottom: height * 0.02,
        ),
        height: height * 0.3,
        width: width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(height * 0.006),
        ),
        child: Container(
          width: width * 0.4,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: height * 0.08,
                width: height * 0.08,
                child: SvgPicture.asset(
                  "assets/mobile_assets/ic_warning_empty.svg",
                  color: Color.fromRGBO(58, 63, 67, 0.5),
                ),
              ),
              SizedBox(
                height: height * 0.02,
              ),
              Text(
                "Для добавления нового животного необходимо заполнить все поля",
                style: TextStyle(fontSize: height * 0.02),
              )
            ],
          ),
        ));
  }

  Widget buttonAddOrganization({var textButton, Function function}) {
    return Container(
      height: height * 0.04,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
            Color.fromRGBO(48, 118, 187, 1),
          ),
          textStyle: MaterialStateProperty.all<TextStyle>(
            TextStyle(
              fontWeight: FontWeight.w900,
            ),
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(height),
            ),
          ),
        ),
        onPressed: () {
          function();
        },
        child: Padding(
          padding: EdgeInsets.all(height * 0.012),
          child: Text(
            "$textButton",
            style: TextStyle(
                fontWeight: FontWeight.w900,
                color: Colors.white,
                fontSize: height * 0.015),
          ),
        ),
      ),
    );
  }

  AdditionObjectMy additionObjectMy = AdditionObjectMy();
  Widget inputData(
      {var text,
      var textEditingController,
      var focusNode,
      var textHint,
      var image}) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          "$text",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: height * 0.019,
          ),
        ),
      ),
      SizedBox(
        height: height * 0.02,
      ),
      Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: width * 0.17,
            height: height * 0.06,
            color: Colors.white,
            child: Form(
              child: TextFormField(
                style: TextStyle(fontSize: height * 0.023),
                focusNode: focusNode,
                controller: textEditingController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(
                      top: height * 0.02,
                      bottom: height * 0.02,
                      left: width * 0.01),
                  hintStyle: TextStyle(fontSize: height * 0.019),
                  hintText: "$textHint",
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: height * 0.003,
                      color: Color.fromRGBO(48, 118, 187, 1),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Color.fromRGBO(58, 63, 67, 0.5),
                        width: height * 0.003),
                  ),
                ),
              ),
              // child: Consumer<T>(
              //     builder: (context, value, child) {
              //   return
              // }),
            ),
          ),
          SizedBox(
            width: width * 0.01,
          ),
          image != null
              ? Container(
                  height: height * 0.05,
                  width: height * 0.05,
                  child: SvgPicture.asset("$image"),
                )
              : Container()
        ],
      ),
    ]);
  }

  Widget rorrrr() {
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            context.read<AddNewCowLogicWeb>().removeWeight();
          },
          onLongPress: () {
            timer = Timer.periodic(Duration(microseconds: 1500), (timer) {
              context.read<AddNewCowLogicWeb>().removeWeight();
            });
          },
          onLongPressUp: () {
            timer.cancel();
          },
          child: Container(
            height: height * 0.06,
            width: height * 0.06,
            child: SvgPicture.asset("assets/mobile_assets/ic_minus.svg"),
          ),
        ),
        GestureDetector(
          // borderRadius: BorderRadius.circular(height),
          onTap: () {
            context.read<AddNewCowLogicWeb>().addWeight();
          },
          onLongPress: () {
            timer = Timer.periodic(Duration(microseconds: 1500), (timer) {
              context.read<AddNewCowLogicWeb>().addWeight();
            });
          },
          onLongPressUp: () {
            timer.cancel();
          },
          child: Container(
            height: height * 0.06,
            width: height * 0.06,
            child: SvgPicture.asset("assets/mobile_assets/ic_plus.svg"),
          ),
        ),
        SizedBox(
          width: width * 0.01,
        ),
        Container(
          color: Colors.white,
          width: width * 0.1,
          height: height * 0.06,
          child: Center(
            child: TextFormField(
              focusNode: context.read<AddNewCowLogicWeb>().focusNodeWeight,
              onChanged: (text) {},
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,4}')),
              ],
              controller: context.read<AddNewCowLogicWeb>().textWeight,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: height * 0.03, fontWeight: FontWeight.w900),
              // focusNode: focusNodeFrom,
              // controller: ,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(
                    top: height * 0.026,
                    bottom: height * 0.008,
                    left: width * 0.001),
                hintStyle: TextStyle(
                    fontSize: height * 0.04, fontWeight: FontWeight.w900),
                // hintText: "$startDay",
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: height * 0.003,
                    color: Color.fromRGBO(48, 118, 187, 1),
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(58, 63, 67, 0.5),
                      width: height * 0.003),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: width * 0.01,
        ),
        Container(
          height: height * 0.05,
          width: height * 0.05,
          child: SvgPicture.asset("assets/mobile_assets/ic_weight.svg"),
        )
      ],
    );
  }

  Widget body() {
    return Container(
      padding: EdgeInsets.only(
        top: height * 0.02,
        left: width * 0.02,
        right: width * 0.05,
        bottom: height * 0.02,
      ),
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
        children: [
          Row(
            children: [
              inputData(
                  textEditingController: textEditingController,
                  textHint: "Введите номер бирки",
                  text: "Присвоенный номер бирки",
                  image: "assets/mobile_assets/ic_tag_filled.svg"),
              SizedBox(
                width: width * 0.013,
              ),
              inputData(
                  textEditingController: textEditingController3,
                  textHint: "Введите номер ошейника",
                  text: "Номер ошейника",
                  image: "assets/mobile_assets/ic_collar.svg"),
              SizedBox(
                width: width * 0.013,
              ),
              inputData(
                  textEditingController: textEditingController1,
                  textHint: "Введите номер бирки",
                  text: "Номер бирки отца",
                  image: "assets/mobile_assets/ic_tag_filled.svg"),
            ],
          ),
          SizedBox(
            height: height * 0.03,
          ),
          Row(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              inputData(
                  textEditingController: textEditingController2,
                  textHint: "Введите номер бирки",
                  text: "Номер бирки матери",
                  image: "assets/mobile_assets/ic_tag_filled.svg"),
              SizedBox(
                width: width * 0.013,
              ),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(
                  "Дата рождения",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: height * 0.019,
                  ),
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                dd(),
              ]),
              SizedBox(
                width: width * 0.013,
              ),
              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Дата рождения",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: height * 0.019,
                        ),
                      ),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      Row(
                        children: [
                          carEEE(
                              index: 0,
                              color: color1,
                              text:
                                  "${context.read<AddNewCowLogicWeb>().nameTelochka}"),
                          carEEE(
                              index: 1,
                              color: color2,
                              text:
                                  "${context.read<AddNewCowLogicWeb>().nameBuc}"),
                        ],
                      ),
                    ]),
              )
            ],
          ),
          SizedBox(
            height: height * 0.03,
          ),
          Expanded(
            child: Container(
                width: width,
                child: Stack(
                  children: [
                    Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            width: width * 0.64,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                checkBox(),
                                SizedBox(
                                  width: width * 0.15,
                                ),
                                buttonAddOrganization(
                                    textButton: "СОХРАНИТЬ",
                                    function: () async {
                                      Person person =
                                          context.read<UserWeb>().person;
                                      if (person
                                              .objectModuls.addition['write'] ==
                                          true) {
                                        context
                                            .read<AddNewCowLogicWeb>()
                                            .saveE();
                                        context
                                            .read<AddNewCowLogicWeb>()
                                            .focusNodeWeight
                                            .unfocus();
                                        context
                                            .read<AddNewCowLogicWeb>()
                                            .functionAddGender(
                                                idGroup: context
                                                    .read<AddNewCowLogicWeb>()
                                                    .idGroup);

                                        additionObjectMy
                                          ..birthday = timeStamp
                                          ..dead = context
                                              .read<AddNewCowLogicWeb>()
                                              .statusDead
                                          ..id_cow = textEditingController.text
                                          ..id_cow_father =
                                              textEditingController1.text
                                          ..id_cow_mother =
                                              textEditingController2.text
                                          ..collar = textEditingController3.text
                                          ..id_gender = context
                                              .read<AddNewCowLogicWeb>()
                                              .gender
                                          ..id_group = context
                                              .read<AddNewCowLogicWeb>()
                                              .idGroup
                                          ..id_species = context
                                              .read<AddNewCowLogicWeb>()
                                              .idSpecies
                                          ..user =
                                              context.read<UserWeb>().person
                                          ..weight = checkText(
                                              text: context
                                                  .read<AddNewCowLogicWeb>()
                                                  .textWeight
                                                  .text)
                                          ..time = timeStamp;

                                        var checkNUll =
                                            provarkaNull(additionObjectMy);

                                        if (checkNUll == null) {
                                          print(
                                              "Проверьте данные которые ввели");
                                          context
                                              .read<AddNewCowLogicWeb>()
                                              .saveE();
                                        } else {
                                          focusNode.unfocus();
                                          focusNode1.unfocus();
                                          focusNode2.unfocus();
                                          context
                                              .read<AddNewCowLogicWeb>()
                                              .textWeight
                                              .text = "0.0";
                                          textEditingController.text = "";
                                          textEditingController1.text = "";
                                          textEditingController2.text = "";
                                          textEditingController3.text = "";

                                          SaveCowAllParametrs
                                              saveCowAllParametrs =
                                              SaveCowAllParametrs();

                                          context
                                              .read<Popopopopopopo>()
                                              .addListAdditionObjectMy(
                                                  additionObjectMy);

                                          context
                                              .read<AddNewCowLogicWeb>()
                                              .functiaFindCowStatus();

                                          var s = await saveCowAllParametrs
                                              .getUserFirebase(
                                                  context
                                                      .read<UserWeb>()
                                                      .person,
                                                  context
                                                      .read<Popopopopopopo>()
                                                      .allParametrsCow);

                                          context
                                              .read<AddNewCowLogicWeb>()
                                              .functiaDontFindCowStatus();
                                          context
                                              .read<AddNewCowLogicWeb>()
                                              .saveE();
                                          context
                                              .read<AddNewCowLogicWeb>()
                                              .valuesOkK();
                                          _controller2.forward();
                                          setState(() {});
                                        }
                                      }
                                    }),
                              ],
                            ),
                          ),
                        ],
                      )
                    ]),
                    Row(
                      children: [
                        modules(
                            textNow:
                                context.read<AddNewCowLogicWeb>().nameGroup,
                            text: "Группа",
                            controllerTransform: controllerTransform,
                            list: context
                                .read<StaticsFirebase>()
                                .allObjectStatic
                                .listGroups,
                            index: 0),
                        SizedBox(
                          width: width * 0.047,
                        ),
                        modules(
                            textNow:
                                context.read<AddNewCowLogicWeb>().nameSpecies,
                            text: "Порода",
                            controllerTransform: controllerTransform1,
                            list: context
                                .read<StaticsFirebase>()
                                .allObjectStatic
                                .listSpecies,
                            index: 1),
                        SizedBox(
                          width: width * 0.047,
                        ),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                "Вес (кг)",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: height * 0.019,
                                ),
                              ),
                              SizedBox(
                                height: height * 0.02,
                              ),
                              rorrrr(),
                            ]),
                      ],
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }

  checkText({String text}) {
    if (text == "") {
      return null;
    } else {
      return double.parse(context.read<AddNewCowLogicWeb>().textWeight.text);
    }
  }

  provarkaNull(AdditionObjectMy additionObjectMy) {
    if (additionObjectMy.dead == null ||
        additionObjectMy.birthday == null ||
        additionObjectMy.id_cow == null ||
        additionObjectMy.id_cow_father == null ||
        additionObjectMy.id_cow_mother == null ||
        additionObjectMy.id_gender == null ||
        additionObjectMy.id_species == null ||
        additionObjectMy.weight == null ||
        additionObjectMy.user == null ||
        textEditingController.text == "" ||
        textEditingController1.text == "" ||
        textEditingController2.text == "") {
      return null;
    } else {
      return additionObjectMy;
    }
  }

  Widget checkBox() {
    return Container(
      child: Consumer<AddNewCowLogicWeb>(
        builder: (BuildContext context, value, Widget child) {
          if (value.idGroup != "UbFdMGuVeySjRTvngpDd") {
            // context.read<AddNewCowLogic>().functionAddGender(
            //     idGroup: context.read<AddNewCowLogic>().idGroup);
            return Container();
          } else {
            return Row(
              children: [
                Container(
                  height: height * 0.05,
                  width: height * 0.05,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(height * 0.006),
                    border: Border.all(
                      width: height * 0.0035,
                      color: Color.fromRGBO(112, 112, 112, 0.5),
                    ),
                  ),
                  child: Consumer<AddNewCowLogicWeb>(
                    builder: (BuildContext context, value, Widget child) {
                      return Checkbox(
                        onChanged: (bool value) {
                          context.read<AddNewCowLogicWeb>().statusDeadBool();
                        },
                        value: value.statusDead,
                      );
                    },
                  ),
                ),
                SizedBox(
                  width: width * 0.01,
                ),
                Text(
                  "Родилось мертвым",
                  style: TextStyle(
                      fontSize: height * 0.019, fontWeight: FontWeight.w500),
                )
              ],
            );
          }
        },
      ),
    );
  }

  void q<T extends AddNewCowLogicWeb>(var value) {
    context.read<T>().functiaSizeInput();
    value.statusSizeInput == true
        ? controllerTransform.forward()
        : controllerTransform.reverse();
  }

  void d<T extends AddNewCowLogicWeb>(var value) {
    context.read<T>().functiaSizeInput1();
    value.statusSizeInput1 == true
        ? controllerTransform1.forward()
        : controllerTransform1.reverse();
  }

  Widget modules(
      {var controllerTransform, var text, var textNow, List list, var index}) {
    return Container(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          "$text",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: height * 0.019,
          ),
        ),
        SizedBox(
          height: height * 0.02,
        ),
        Container(
          child: Consumer<AddNewCowLogicWeb>(builder: (context, value, child) {
            if (index == 0) {
              if (value.statusSizeInput == true) {
                return staticData.conter(
                  context: context,
                  width: width,
                  textNow: context.read<AddNewCowLogicWeb>().nameGroup,
                  widgetListBool: true,
                  widgetList: index == 0
                      ? functiaListGroup(list)
                      : functiaListSpecies(list),
                  height: height,
                  statusAnim: true,
                  controllerTransform: controllerTransform,
                  onTTap: () => index == 0
                      ? q<AddNewCowLogicWeb>(value)
                      : d<AddNewCowLogicWeb>(value),
                );
              } else {
                return staticData.conter(
                  context: context,
                  width: width,
                  textNow: context.read<AddNewCowLogicWeb>().nameGroup,
                  widgetListBool: true,
                  widgetList: index == 0
                      ? functiaListGroup(list)
                      : functiaListSpecies(list),
                  height: height,
                  statusAnim: false,
                  controllerTransform: controllerTransform,
                  onTTap: () => index == 0
                      ? q<AddNewCowLogicWeb>(value)
                      : d<AddNewCowLogicWeb>(value),
                );
              }
            } else {
              if (value.statusSizeInput1 == true) {
                return staticData.conter(
                  context: context,
                  width: width,
                  textNow: context.read<AddNewCowLogicWeb>().nameSpecies,
                  widgetListBool: true,
                  widgetList: index == 0
                      ? functiaListGroup(list)
                      : functiaListSpecies(list),
                  height: height,
                  statusAnim: true,
                  controllerTransform: controllerTransform,
                  onTTap: () => index == 0
                      ? q<AddNewCowLogicWeb>(value)
                      : d<AddNewCowLogicWeb>(value),
                );
              } else {
                return staticData.conter(
                  context: context,
                  width: width,
                  textNow: context.read<AddNewCowLogicWeb>().nameSpecies,
                  widgetListBool: true,
                  widgetList: index == 0
                      ? functiaListGroup(list)
                      : functiaListSpecies(list),
                  height: height,
                  statusAnim: false,
                  controllerTransform: controllerTransform,
                  onTTap: () => index == 0
                      ? q<AddNewCowLogicWeb>(value)
                      : d<AddNewCowLogicWeb>(value),
                );
              }
            }
          }),
        ),
      ]),
    );
  }

  functiaListGroup(List<Groups> listUserSubOrganization) {
    ScrollController controllerOne = ScrollController();
    return Container(
      child: Scrollbar(
        controller: controllerOne,
        isAlwaysShown: true,
        child: GridView.builder(
          itemCount: listUserSubOrganization.length,
          controller: controllerOne,
          padding: EdgeInsets.all(0),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: height * 0.02,
              width: width,
              child: InkWell(
                onTap: () {
                  context.read<AddNewCowLogicWeb>().saveGroup(
                      idGroup: listUserSubOrganization[index].id_groups,
                      nameGroup: listUserSubOrganization[index].name);
                  context.read<AddNewCowLogicWeb>().functiaSizeInput();
                  controllerTransform.reverse();
                  context.read<AddNewCowLogicWeb>().functionAddGender(
                      idGroup: context.read<AddNewCowLogicWeb>().idGroup);
                },
                child: Text(
                  "${listUserSubOrganization[index].name}",
                  style: TextStyle(
                    fontSize: height * 0.021,
                    color: Color.fromRGBO(58, 63, 67, 0.8),
                  ),
                ),
              ),
            );
          },
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1, mainAxisExtent: height * 0.05),
        ),
      ),
    );
  }

  functiaListSpecies(List<Species> listUserSubOrganization) {
    ScrollController controllerOne = ScrollController();
    return Container(
      child: Scrollbar(
        controller: controllerOne,
        isAlwaysShown: true,
        child: GridView.builder(
          itemCount: listUserSubOrganization.length,
          controller: controllerOne,
          padding: EdgeInsets.all(0),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: height * 0.02,
              width: width,
              child: InkWell(
                onTap: () {
                  context.read<AddNewCowLogicWeb>().functionSaveSubOrganization(
                      idSpecies: listUserSubOrganization[index].id_species,
                      nameSpecies: listUserSubOrganization[index].name);
                  context.read<AddNewCowLogicWeb>().functiaSizeInput1();
                  controllerTransform1.reverse();
                },
                child: Text(
                  "${listUserSubOrganization[index].name}",
                  style: TextStyle(
                    fontSize: height * 0.021,
                    color: Color.fromRGBO(58, 63, 67, 0.8),
                  ),
                ),
              ),
            );
          },
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1, mainAxisExtent: height * 0.05),
        ),
      ),
    );
  }

  var color1 = Color.fromRGBO(48, 118, 187, 1);
  var color2;
  Widget carEEE({var text, var color, var index}) {
    var d = context.read<StaticsFirebase>().allObjectStatic.listGenders;
    return InkWell(
      onTap: () {
        if (text == "Бык") {
          context.read<AddNewCowLogicWeb>().gender = "7LGy1b7FncEaJNKX50CN";
        } else if (text == "Телка") {
          context.read<AddNewCowLogicWeb>().gender = "vkYVpkLimiyoYGXeSJKr";
        } else if (text == "Телочка") {
          context.read<AddNewCowLogicWeb>().gender = "QxlG49dRxQrQg9H0RnDE";
        } else {
          context.read<AddNewCowLogicWeb>().gender = "swLS6KyxrzBu0NEtgTgx";
        }
        color1 == Color.fromRGBO(48, 118, 187, 1)
            ? color1 = null
            : color1 = Color.fromRGBO(48, 118, 187, 1);
        color2 == Color.fromRGBO(48, 118, 187, 1)
            ? color2 = null
            : color2 = Color.fromRGBO(48, 118, 187, 1);

        setState(() {});
      },
      child: Container(
        width: width * 0.085,
        height: height * 0.06,
        decoration: BoxDecoration(
          color: color,
          border: Border.all(
            width: height * 0.003,
            color: Color.fromRGBO(58, 63, 67, 0.5),
          ),
          borderRadius: BorderRadius.only(
            topLeft: index == 0
                ? Radius.circular(height * 0.006)
                : Radius.circular(0),
            bottomLeft: index == 0
                ? Radius.circular(height * 0.006)
                : Radius.circular(0),
            topRight: index == 1
                ? Radius.circular(height * 0.006)
                : Radius.circular(0),
            bottomRight: index == 1
                ? Radius.circular(height * 0.006)
                : Radius.circular(0),
          ),
        ),
        child: Center(child: Consumer<AddNewCowLogicWeb>(
          builder: (BuildContext context, value, Widget child) {
            if (index == 0) {
              return Text(
                "${value.nameTelochka}",
                style: TextStyle(
                    color: color == null ? Colors.black : Colors.white,
                    fontSize: height * 0.02,
                    fontWeight: FontWeight.w600),
              );
            } else {
              return Text(
                "${value.nameBuc}",
                style: TextStyle(
                    color: color == null ? Colors.black : Colors.white,
                    fontSize: height * 0.02,
                    fontWeight: FontWeight.w600),
              );
            }
          },
        )),
      ),
    );
  }

  var startTimeStamp;
  Widget dd() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: width * 0.17,
          height: height * 0.06,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(height * 0.006),
            border: Border.all(
              width: height * 0.003,
              color: Color.fromRGBO(58, 63, 67, 0.5),
            ),
          ),
          child: Center(
            child: Row(
              children: [
                Container(
                    width: width * 0.05,
                    child: Center(
                      child: Text(
                        startDay,
                        style: TextStyle(
                          fontSize: height * 0.02,
                        ),
                      ),
                    )),
                Container(
                  color: Color.fromRGBO(58, 63, 67, 0.5),
                  height: height * 0.035,
                  width: width * 0.001,
                ),
                Container(
                  width: width * 0.05,
                  child: Center(
                    child: Text(
                      startMounth,
                      style: TextStyle(
                        fontSize: height * 0.02,
                      ),
                    ),
                  ),
                ),
                Container(
                  color: Color.fromRGBO(58, 63, 67, 0.5),
                  height: height * 0.035,
                  width: width * 0.001,
                ),
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      var d = await DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(2010, 1, 0),
                          maxTime: DateTime(2030, 12, 0), onChanged: (date) {
                        return date;
                        // putStartDay(
                        //     day: date.day,
                        //     mounth: date.month,
                        //     yers: date.year);
                      }, onConfirm: (date) {
                        startTimeStamp = date.millisecondsSinceEpoch;
                        return date;
                        // putStartDay(
                        //     day: date.day,
                        //     mounth: date.month,
                        //     yers: date.year);
                      },
                          currentTime: DateTime(
                              DateTime.now().year, DateTime.now().month, 1),
                          locale: LocaleType.ru);

                      startDay = d.day == null ? startDay : d.day.toString();
                      startMounth =
                          d.month == null ? startMounth : d.month.toString();
                      startYers =
                          d.year == null ? startYers : d.year.toString();

                      setState(() {});
                    },
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            startYers,
                            style: TextStyle(
                              fontSize: height * 0.02,
                            ),
                          ),
                          Container(
                            height: height * 0.023,
                            width: height * 0.023,
                            child: SvgPicture.asset(
                                "assets/mobile_assets/ic_arrow_down.svg"),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          width: width * 0.01,
        ),
        Container(
          height: height * 0.05,
          width: height * 0.05,
          child:
              SvgPicture.asset("assets/mobile_assets/ic_calendar_filled.svg"),
        )
      ],
    );
  }
}
